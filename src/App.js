import React from 'react';
import { Route,Routes,useLocation,Outlet } from 'react-router-dom';
import Login from './components/login/login';
import SignUp from './components/login/signup';
import Home from './components/home/home';
import FilmInfor from './components/filmInfor/filmInfor';
import Header from './components/layout/header/header';
import Footer from './components/layout/footer/header';
import User from './components/user/user';
import Available from './components/available/available';
import Cinema from './components/cinema/cinema';
import Preferential from './components/preferential/preferential';
import News from './components/news/news';
import Booking from './components/booking/booking';
import Thanks from './components/booking/thanks';
function Dashboard() {
  return (
    <div>
      <Header/>
      <Outlet />
      <Footer/>
    </div>
  );
}
function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Dashboard/>}>
          <Route path="/" element={<Home/>}/>
          <Route path="/:id" element={<FilmInfor/>}/>
          <Route path="/user" element={<User/>}/>
          <Route path="/available" element={<Available/>}/>
          <Route path="/cinema" element={<Cinema/>}/>
          <Route path="/preferential" element={<Preferential/>}/>
          <Route path="/news" element={<News/>}/>
          <Route path="/:id/booking" element={<Booking/>}/>
          <Route path="/thanks" element={<Thanks/>}/>
        </Route>
        <Route path="/login" element={<Login/>}/>
        <Route path="/signup" element={<SignUp/>}/>
      </Routes>
    </div>
  );
}

export default App;