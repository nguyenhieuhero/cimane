export var fakeNews=[
    {
        name:"Jackie Chan Says Rush Hour 4 Is (Still) In The Works",
        img:"https://images.bauerhosting.com/empire/2022/12/rush-hour-main.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"A fourth Rush Hour movie is something that has occasionally pinged the news radar, for different reasons. In June of 2012, star Jackie Chan was saying he didn't think it would happen, only for producer Arthur Sarkissian to strike a more positive tone the following month. Now, a decade later, it's Chan who is saying that it's more likely."
    },
    {
        name:"Studio Ghibli Confirms Hayao Miyazaki's New Film How Do You Live For 2023",
        img:"https://images.bauerhosting.com/legacy/media/5ebe/6302/ecdb/c703/de18/1b65/hayao-miyazaki.jpg?q=80&w=600&ar=16:9&fit=crop&crop=top",
        content:"It's finally happening: a new Hayao Miyazaki film is on the release slate. A decade on from his supposed retirement (2013's incredible The Wind Rises was intended as his swansong), the Studio Ghibli co-founder is about to unveil a brand new feature film - one that's been long-gestating in the studio. Little has been known about the project, other than awed whispers of its ambition and devotion to tradition hand-drawn animation techniques - but now, according to the official Studio Ghibli social media accounts, it's preparing for release next summer."
    },
    {
        name:"Composer Angelo Badalamenti Dies, Aged 85",
        img:"https://images.bauerhosting.com/empire/2022/12/angelo-badalamenti.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"Angelo Badalamenti, a composer with a considerable career, and who might be most famous for his collaborations with David Lynch, has died. He was 85."
    },
    {
        name:"The Banshees Of Inisherin, Everything Everywhere All At Once And The Fabelmans Lead The 80th Golden Globe Nominations",
        img:"https://images.bauerhosting.com/empire/2022/12/banshees-everything-fabelmans.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"Still more notorious than famous at this point for the many scandals involving the Hollywood Foreign Press Association which hands them out, the Golden Globes didn't even happen in person this year. Still, they're pushing ahead the 2023 show and the nominations are out, with movies such as The Banshees Of Inisherin, Tár, Everything Everywhere All At Once and more all notching a healthy batch of nods."
    },
    {
        name:"Director Todd Phillips Debuts First Look At Joker: Folie À Deux",
        img:"https://images.bauerhosting.com/empire/2022/08/joker-folie-a-deux-teaser.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"The cameras are now rolling on Joker sequel Joker: Folie À Deux. How do we know? Credit co-writer Todd Phillips, who has posted a first look at Joaquin Phoenix back in wiry Arthur Fleck form for the new movie."
    },
    {
        name:"Angourie Rice And Auli'i Cravalho Join The Mean Girls Musical Movie",
        img:"https://images.bauerhosting.com/empire/2022/12/rice-cravalho.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"Fetch: still not happening. But the Mean Girls movie musical adaptation? Definitely a thing. Casting continues on the film, with Angourie Rice, Reneé Rapp, Auli'i Cravalho and Jaquel Spivey all joining the ensemble,."
    },
    {
        name:"Daniel Craig Starring For Luca Guadagnino In Adaptation Of William S. Burroughs' Queer",
        img:"https://images.bauerhosting.com/empire/2022/12/daniel-craig.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"After a quick — a very quick - run in cinemas, Daniel Craig will be on Netflix screens as Benoit Blanc in Rian Johnson's sequel Glass Onion: A Knives Out Mystery. But Craig also has his eye on a potential future film, signing up to star for Luca Guadagnino in an adaptation of William S. Burroughs' Queer."
    },
    {
        name:"Empire Podcast #544: Lee Unkrich, Rebecca Hall, Antoine Fuqua, Letitia Wright & Tamara Lawrance",
        img:"https://images.bauerhosting.com/empire/2022/12/unkrich-hall-fuqua-lawrence-wright.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"Christmas is coming, folks, and that means that the Empire Podcast's sack is simply heaving with guests this week. First, Toy Story 3 and Coco director, Lee Unkrich, pops into the booth for a bit to talk about his new, definitive book about the making of Stanley Kubrick's The Shining, and tackle this week's listener question (about Christmas songs in movies). Then Chris Hewitt chats with Resurrection star Rebecca Hall and Emancipation director Antoine Fuqua, and Amon Warmann gets to the heart of The Silent Twins' silent twins, Letitia Wright and Tamara Lawrance. And yes, he asks about Black Panther: Wakanda Forever."
    },
    {
        name:"Taylor Swift To Direct Her Feature Debut For Searchlight Pictures",
        img:"https://images.bauerhosting.com/empire/2022/12/taylor-swift.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"It's her. Hi. She's the director, it's her. Yes, Swifties around the globe, rejoice - it has been announced that pop superstar Taylor Swift is directing her very first feature for Searchlight Pictures, based on an original script also written by her. No plot or potential casting details have been released just yet."
    },
    {
        name:"James Gunn Comments On Reports Of DC Studios' Future",
        img:"https://images.bauerhosting.com/empire/2022/10/james-gunn-main.jpg?q=80&w=1400&ar=16:9&fit=crop&crop=top",
        content:"On Wednesday,a story broke about recently hired DC Studio bosses James Gunn and Peter Safran turning down director Patty Jenkins' recent treatment for a third Wonder Woman movie and essentially putting her take on the character on ice. The story, from The Hollywood Reporter had a mixture of speculation on what it could mean for other roles, such as Henry Cavill as Superman. Gunn, who is known for being accessible to fans on social media, has now hit twitter to comment on the story.",
    },
]