import styles from './filmHome.module.css'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useNavigate } from 'react-router-dom';
export default function FilmHome(prop){
    // console.log(prop)
    const navigate=useNavigate()
    return <div className={styles.container} onClick={()=>{navigate(`/${prop.prop.engName}`,{state:prop.prop})}}>
        <div className={styles.filmImg} style={{backgroundImage: `url(${prop.prop.url.imgUrl})`,height:"350px",backgroundSize:"cover"}}/>
        <h2>{prop.prop.name}</h2>
        <p className={styles.propName}>Thể loại: <span>{prop.prop.type}</span></p>
        <p className={styles.propName}>Thời lượng: <span>{prop.prop.duration}</span></p>
    </div>
}