import styles from './filmBanner.module.css'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useNavigate } from 'react-router-dom';
export default function Film(prop){
    // console.log(prop)
    const navigate=useNavigate()
    return <div className={styles.container}>
        <div className={styles.filmImg} style={{backgroundImage: `url(${prop.prop.url.imgUrl})`,height:"354px",backgroundSize:"cover"}}>
            <div className={styles.filmOption}>
                <div className={styles.trailerArea}>
                    <div className={styles.trailerButton} onClick={()=>prop.modalState([true,prop.prop.url.trailerUrl])}>
                        <ArrowForwardIcon/>
                        <p>Trailer</p>
                    </div>
                </div>
                <div className={styles.bookingArea}>
                    <div className={styles.bookingAreaBG}/>
                    <h3 className={styles.filmName}>{prop.prop.name}</h3>
                    <div className={styles.buttonArea}>
                        {/* <Link to={`/film-infor/${prop.prop.engName}`} className={styles.bookingButton}><p>Xem chi tiet</p></Link> */}
                        <p className={styles.bookingButton} onClick={()=>{navigate(`/${prop.prop.engName}`,{state:prop.prop})}}>Xem chi tiết</p>
                        {/* <Link to="/booking" className={styles.bookingButton}><p>Dat ve</p></Link> */}
                        <p className={styles.bookingButton} onClick={()=>{navigate(`/${prop.prop.engName}/booking`,{state:prop.prop})}}>Đặt vé</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
}