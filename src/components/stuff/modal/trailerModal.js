import styles from './trailerModal.module.css'
export default function Trailer(prop){
    return <div className={styles.trailerVideo}>
        <iframe width="640" height="390" src={prop.url} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    </div>
}