import styles from "./newsHome.module.css"
export default function NewsHome(prop){
    return <div className={styles.newsBanner}>
        <img src={prop.prop.img} width="100%"/>
        <div>
            <h3>{prop.prop.name}</h3>
            <p>{prop.prop.content}</p>
        </div>
    </div>
}