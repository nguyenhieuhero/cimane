import styles from "./newsBanner.module.css"
export default function NewsBanner(prop){
    console.log(prop)
    return <div className={styles.newsBanner}>
        <img src={prop.prop.img} width="100%"/>
        <h3>{prop.prop.name}</h3>
        <p>{prop.prop.content}</p>
    </div>
}