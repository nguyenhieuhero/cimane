import styles from './home.module.css'

import { useEffect, useState } from 'react';
import SideContent from '../layout/sideContent/sideContent';
import Slide from './slide/slide';
import Slide2 from './slide/slide2';
import Slide3 from './slide/slide3';
import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined';
import Trailer from '../stuff/modal/trailerModal';
import { Outlet } from 'react-router-dom';
import axios from 'axios';
import { fakeNews } from '../stuff/fakeData/fakeNews';
import { fakeDiscount } from '../stuff/fakeData/fakeDiscount';
import { fakeEvent } from '../stuff/fakeData/fakeEvent';
export default function Home(){
    const [data,setData] = useState()
    const [trailerState,setTrailerState]=useState([false,""])
    // console.log(trailerState)
    const getData = () => {
        axios.get("http://localhost:8000/film/")
        .then(res => {
            setData(res.data)
        })
    }
    useEffect(()=>{
        getData()
    },[])
    // console.log(data)
    return <>
        <SideContent name="late-shift"/>
        <div className={styles.home}>
            <Slide/>
            <div className={styles.box1Name}>
                <div className={styles.box1Line}/>
                <h1>Phim đang chiếu</h1>
            </div>
            <Slide2 data={data} modalState={setTrailerState}/>
            <div className={styles.box1Name}>
                <div className={styles.box1Line}/>
                <h1>Tin tức nổi bật</h1>
            </div>
            <Slide3 data={fakeNews}/>
            <div className={styles.box1Name}>
                <div className={styles.box1Line}/>
                <h1>Sự kiện</h1>
            </div>
            <div className={styles.dicountArea}>
                {fakeDiscount.map(value=><img key={value} src={value}/>)}
            </div>
            <div className={styles.dicountArea}>
                {fakeEvent.map(value=><img key={value} src={value}/>)}
            </div>
        </div>
        {trailerState[0]&&<div className={styles.trailerSpace}>
            <div className={styles.trailerBG}/>
            <div className={styles.trailerBox}>
                <div onClick={()=>setTrailerState(false)}>
                    <ClearOutlinedIcon className={styles.closeTrailerButton} sx={{fontSize:25}}/>
                </div>
                <Trailer url={trailerState[1]}/>
            </div>
        </div>}
    </>
}