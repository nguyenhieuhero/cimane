import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/autoplay";
import NewsHome from "../../stuff/news/newsHome";

// import required modules
import SwiperCore,{ Autoplay, Pagination, Navigation } from "swiper";
import styles from "../slide/slide.module.css"


export default function Slide3(prop){
    // console.log(prop)
    SwiperCore.use([Autoplay]);
    return <div className={styles.slideWrapper}>
        <div className={styles.slider}>
            <Swiper
            slidesPerView={1}
            spaceBetween={6}
            loop={true}
            pagination={{
              clickable: true,
            }}
            autoplay={{
                delay:4000,
                disableOnInteraction: false,
                pauseOnMouseEnter: true,
            }}
            style={{
                "--swiper-navigation-color": "#FFBE00",
                "--swiper-pagination-color": "#FFBE00",
                "--swiper-pagination-bullet-inactive-color":"gray",
                "--swiper-pagination-bullet-inactive-opacity": "0.5",
                "--swiper-pagination-bullet-size": "14px",
            }}
            className="mySwiper"
            >
                {/* {data.map((img,index)=><SwiperSlide key={index}><img src={img.img} className={styles.img}/></SwiperSlide>)} */}
                {prop.data?.map((infor,index)=><SwiperSlide key={index}><NewsHome prop={infor}/></SwiperSlide>)}
            </Swiper>
        </div>
    </div>
}