import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/autoplay";


// import required modules
import SwiperCore,{ Autoplay, Pagination, Navigation } from "swiper";
import styles from "./slide.module.css"


export default function Slide(prop){
    SwiperCore.use([Autoplay]);
    const data = [
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/l/a/late_shift_-_rolling_banner_980x448.jpg"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/9/8/980wx448h_18.jpg"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/h/p/hpmposter_w980xh448_2.jpg"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/9/8/980_x_448__10.jpg"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/r/o/rolling_banner_980wx448h_1_1.jpg"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/9/8/980x448_64.png"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/r/o/rolling_banner_980x448_5.jpg"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/9/8/980x4480_rolling_banner.jpg"},
        {img:"https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/9/8/980x448_176.jpg"},
    ]
    return <div className={styles.slideWrapper} style={{background:"url('https://www.cgv.vn/skin/frontend/cgv/default/images/bg_c_bricks.png')"}}>
        <div className={styles.slider}>
            <Swiper
            slidesPerView={1}
            spaceBetween={0}
            loop={true}
            pagination={{
              clickable: true,
            }}
            navigation={true}
            modules={[Pagination, Navigation]}
            autoplay={{
                delay:2500,
                disableOnInteraction: false,
                pauseOnMouseEnter: true,
            }}
            style={{
                "--swiper-navigation-color": "#FFBE00",
                "--swiper-pagination-color": "#FFBE00",
                "--swiper-pagination-bullet-inactive-color":"gray",
                "--swiper-pagination-bullet-inactive-opacity": "0.5",
                "--swiper-pagination-bullet-size": "14px",
            }}
            className="mySwiper"
            >
                {data.map((img,index)=><SwiperSlide key={index}><img src={img.img} className={styles.img}/></SwiperSlide>)}
            </Swiper>
        </div>
    </div>
}