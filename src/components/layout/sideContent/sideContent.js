import styles from "./sideContent.module.css"
import { useNavigate } from "react-router-dom"
import axios from "axios";
import { useState,useEffect } from "react";

export default function SideContent(prop){
    const [data,setData] = useState()
    const navigate = useNavigate();
    const getData = () => {
        axios.get(`http://localhost:8000/film/${prop.name}`)
        .then(res => {
            setData(res.data)
        })
    }
    useEffect(()=>{
        getData()
    },[])
    // console.log(data)
    return <div className={styles.side}>
        <div className={styles.sideContent} onClick={()=>{navigate(`/${data[0].engName}`,{state:data[0]})}}>
            <img src="https://www.cgv.vn/media/wysiwyg/2022/112022/120x600_.png" height="550px"/>
        </div>
    </div>
}