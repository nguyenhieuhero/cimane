import styles from './footer.module.css'
import { useNavigate } from 'react-router-dom'
import FacebookIcon from '@mui/icons-material/Facebook';
import EmailIcon from '@mui/icons-material/Email';
import YouTubeIcon from '@mui/icons-material/YouTube';
import twitch from './twitch.png'
export default function Footer(){
    const navigate=useNavigate()
    return <div className={styles.footer}>
        <div>
            <p>Designed by Nguyễn Minh Hiếu</p>
            <p>Tham khảo thiết kết từ: <a href='https://www.cgv.vn/'>CGV</a></p>
            <p>Email: nguyenhieuhero16@gmail.com</p>
        </div>
            <a href='https://www.facebook.com/profile.php?id=100004749408014'><FacebookIcon sx={{color:"antiquewhite",fontSize:50,cursor:"pointer"}}/></a>
            <a href='https://www.youtube.com/channel/UCn-LYukdkgT2ndgcGAuBl6w'><YouTubeIcon sx={{color:"#FF0000",fontSize:50,cursor:"pointer"}}/></a>
            <a href='https://www.twitch.tv/nguyenhieuhero'><img src={twitch} height="40px"/></a>
    </div>
}