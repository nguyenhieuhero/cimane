import styles from './header.module.css'
import { Navigate, useLocation, useNavigate, Link } from 'react-router-dom';
import { useState,useEffect } from 'react';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import NotificationsOffIcon from '@mui/icons-material/NotificationsOff';
import { NavLink } from 'react-router-dom';
export default function Header(){
    const [userShow,setUserShow]=useState(false)
    const [notification,setNotification]=useState(false)
    const navigate=useNavigate()
    const {pathname}=useLocation()

    const handleLogout = ()=>{
        localStorage.removeItem("logincimane")
        navigate("/login")
    }
    useEffect(()=>{
        setUserShow(false)
    },[pathname])

    return <div>
        <div className={styles.banner}>
            <Link to="/" className={styles.logo}>Cimane</Link>
            <NavLink to="/available" className={styles.nav}>Phim</NavLink>
            {/* <NavLink to="/cinema" className={styles.nav}>Rạp</NavLink> */}
            <NavLink to="/news" className={styles.nav}>News</NavLink>
            <NavLink to="/preferential" className={styles.nav}>Ưu đãi</NavLink>
            {localStorage.getItem("logincimane")?<><div className={styles.userArea}>
                {notification?<NotificationsActiveIcon className={styles.bellIcon} onClick={()=>setNotification(!notification)}/>:
                <NotificationsOffIcon className={styles.bellIcon} onClick={()=>setNotification(!notification)}/>}
                <div className={styles.userLogo}
                onClick={()=>setUserShow(!userShow)}
                />
            </div>
            {userShow&&<div className={styles.userMethod}>
                <h3 onClick={()=>navigate("/user")}>Profile</h3>
                <h3>Thiết lập</h3>
                <h3>Phản hồi</h3>
                <h3 onClick={()=>handleLogout()}>Đăng xuất</h3>
            </div>}</>:<div className={styles.userArea}><h4 onClick={()=>navigate("/login")}>Đăng nhập/Đăng ký</h4></div>}
        </div>
    </div>
}