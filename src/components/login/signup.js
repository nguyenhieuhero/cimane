import { useState,useEffect } from 'react';
import { FaEye,FaEyeSlash } from "react-icons/fa";
import { useNavigate, } from 'react-router-dom';
import * as yup from "yup";
import {Formik} from "formik"
import axios from "axios";
import styles from './signup.module.css';
import { Link } from 'react-router-dom';

export default function SignUp() {
  const navigate = useNavigate()
  const signUpValidationSchema = yup.object({
    phone: yup.string().required().matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g,"Sai định dạng"),
    password: yup.string().required(),
    confirm: yup.string().oneOf([yup.ref('password'), null],"Không trùng mật khẩu"),
    name: yup.string().required(),
    birthday: yup.date().required(),
    email: yup.string().email().required(),
    address: yup.string().required(),
    gender: yup.string().required(),
  })

  useEffect(()=>{
    if(localStorage.getItem("logincimane")){
      alert("Bạn đã đăng nhập!")
      navigate("../")
    }
  },[])

  const handleSignUp = async data => {
    try {
      // console.log(data)
      const res = await axios.post("http://localhost:8000/login",data)
      console.log(res)
      if(res.data.success){
        alert("Tạo tài khoản thành công")
        navigate("../login")
      }
    } catch (error) {
      console.log(error)
    }
  }



  return <div className={styles.loginPage}>
    <div className={styles.formSpace}>
      <h1 className={styles.logo}>Cimane</h1>
      <div className={styles.formMethodSpace}>
      </div>
        <Formik 
        validationSchema={signUpValidationSchema}
        initialValues={{phone:"",password:"",name:"",birthday:"",email:"",address:"",gender:"",confirm:""}}
        onSubmit={data=>handleSignUp(data)}
        >
        {({handleChange, handleSubmit, handleBlur, errors, values}) => (
          <form className={styles.form} onSubmit={handleSubmit}>
            <h1>Đăng ký</h1>
            <input className={styles.input}
            name="name" type="text"
            onChange={handleChange("name")}
            onBlur={handleBlur("name")}
            value={values.name}
            placeholder="Họ và tên"
            />
            <input className={styles.input}
            name="phone" type="text"
            onChange={handleChange("phone")}
            onBlur={handleBlur("phone")}
            value={values.phone}
            placeholder="Số điện thoại"
            />
            <input className={styles.input}
            name="email" type="email"
            onChange={handleChange("email")}
            onBlur={handleBlur("email")}
            value={values.email}
            placeholder="Email"
            />
            <input className={styles.input}
            name="password" type="password"
            onChange={handleChange("password")}
            onBlur={handleBlur("password")}
            value={values.password}
            placeholder="Password"
            />
            <input className={styles.input}
            name="confirm" type="password"
            onChange={handleChange("confirm")}
            onBlur={handleBlur("confirm")}
            value={values.confirm}
            placeholder="Confirm Password"
            />
            <input className={styles.input}
            name="birthday" type="text"
            onChange={handleChange("birthday")}
            onBlur={handleBlur("birthday")}
            value={values.birthday}
            placeholder="Ngày sinh MM/dd/YYYY"
            />
            <input className={styles.input}
            name="address" type="text"
            onChange={handleChange("address")}
            onBlur={handleBlur("address")}
            value={values.address}
            placeholder="Địa chỉ"
            />
            {/* <input className={styles.input}
            name="gender" type="text"
            onChange={handleChange("gender")}
            onBlur={handleBlur("gender")}
            value={values.gender}
            placeholder="Giới tính"
            /> */}
            <div className={styles.radioInput}>
              Nam <input type="radio" value="nam" name="gender" defaultChecked onChange={handleChange("gender")}/>
              Nữ <input type="radio" value="nữ" name="gender" onChange={handleChange("gender")}/>
            </div>
            <button type = "submit" className={styles.submitButton} onClick={()=>handleSignUp()}>Đăng ký</button>
            <Link to="../login">Đã có tài khoản?</Link>
          </form>
        )}
        </Formik>
    </div>
  </div>
  }