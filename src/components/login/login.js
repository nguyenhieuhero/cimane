import { useState,useEffect } from 'react';
import { FaEye,FaEyeSlash } from "react-icons/fa";
import { useNavigate, } from 'react-router-dom';
import * as yup from "yup";
import {Formik} from "formik"
import axios from "axios";
import styles from './login.module.css';
import { Link } from 'react-router-dom';
import { Alert } from '@mui/material';

export default function Login() {
  const navigate = useNavigate()
  const loginValidationSchema = yup.object({
    phone: yup.string().required("Bạn chưa nhập sđt").matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g,"Sai định dạng"),
    password: yup.string().required("Bạn chưa nhập mật khẩu"),
  })

  useEffect(()=>{
    if(localStorage.getItem("logincimane")){
      alert("Bạn đã đăng nhập!")
      navigate("../")
    }
  },[])

  const handleSignIn = async data => {
    try {
      console.log(data)
      const res = await axios.post("http://localhost:8000/login/auth",data)
      console.log(res)
      if(res.data.success){ 
        // console.log(res.data.user)
        localStorage.setItem("logincimane",JSON.stringify(res.data.user))
        navigate("../")
      }
      else{
        alert(res.data.message)
      }
    } catch (error) {
      console.log(error)
    }
  }



  return <div className={styles.loginPage}>
    <div className={styles.formSpace}>
      <h1 className={styles.logo} onClick={()=>navigate("../")}>Cimane</h1>
      <div className={styles.formMethodSpace}>
      </div>
        <Formik 
        validationSchema={loginValidationSchema}
        initialValues={{phone:"",password:""}}
        onSubmit={data=>handleSignIn(data)}
        >
        {({handleChange, handleSubmit, handleBlur, errors, values}) => (
          <form className={styles.form} onSubmit={handleSubmit}>
            <h1>Đăng nhập</h1>
            <input className={styles.input}
            name="phone" type="text"
            onChange={handleChange("phone")}
            onBlur={handleBlur("phone")}
            value={values.phone}
            placeholder="Số điện thoại"
            />
            {errors.phone && <p>{errors.phone}</p>}
            <input className={styles.input}
            name="password" type="password"
            onChange={handleChange("password")}
            onBlur={handleBlur("password")}
            value={values.password}
            placeholder="Password"
            />
            {errors.password && <p>{errors.password}</p>}
            <button type = "submit" className={styles.submitButton}>Đăng nhập</button>
            <Link to="../signup">Chưa có tài khoản?</Link>
          </form>
        )}
        </Formik>
    </div>
  </div>
  }