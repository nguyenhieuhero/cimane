import styles from "./user.module.css"
import { useNavigate } from "react-router-dom"
export default function User(){
    const userInfor=JSON.parse(localStorage.getItem("logincimane"))
    const navigate=useNavigate()
    console.log(userInfor)
    return <div className={styles.user}>
        <div style={{display:"flex",flexDirection:"row"}}>
            <div>
                <img src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png" height="200px"/>
                <p style={{textAlign:"center"}}>Đổi avatar</p>
            </div>
            <div className={styles.userInfor}>
                <p className={styles.title}>Thông tin cá nhân</p>
                <div className={styles.dataWrapper}>
                    <div className={styles.userData}>
                        <p>Tên</p>
                        <h3>{userInfor.name}</h3>
                        <p>Điện thoại</p>
                        <h3>{userInfor.phone}</h3>
                        <p>Giới tính</p>
                        <h3>{userInfor.gender}</h3>
                        <p>Ngày sinh</p>
                        <h3>{userInfor.birthday}</h3>
                        <p>Địa chỉ email</p>
                        <h3 className={styles.email}>{userInfor.email}</h3>
                    </div>
                    <div className={styles.userData}>
                        <p>Địa chỉ</p>
                        <h3>{userInfor.address}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.bookingArea}>
            <div className={styles.bookingButton} onClick={()=>{navigate("../")}}>Trở về trang chủ</div>
            <div className={styles.bookingButton} onClick={()=>alert("Tính năng đang bảo trì")}>Cập nhật thông tin</div>
        </div>
    </div>
}