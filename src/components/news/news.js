import styles from "./news.module.css"
import NewsBanner from "../stuff/news/newsBanner"
import NewsHome from "../stuff/news/newsHome"
import { fakeNews } from "../stuff/fakeData/fakeNews"
export default function News(){
    return <div className={styles.news}>
        {fakeNews.map(data=><NewsBanner key={data.name} prop={data}/>)}
    </div>
}