import styles from "./available.module.css"
import { useState,useEffect } from "react"
import axios from "axios"
import FilmHome from "../stuff/film/filmHome"
export default function Available(){
    const [data,setData] = useState()
    const getData = () => {
        axios.get("http://localhost:8000/film/")
        .then(res => {
            setData(res.data)
        })
    }
    useEffect(()=>{
        getData()
    },[])
    console.log(data)
    return <div className={styles.available}>
        {data?.map(value=><FilmHome prop={value}/>)}
    </div>
}