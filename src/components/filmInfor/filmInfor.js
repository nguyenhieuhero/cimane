import styles from "./filmInfor.module.css"
import { useLocation } from "react-router-dom"
import { useNavigate } from "react-router-dom";
export default function FilmInfor(){
    const {state} = useLocation();
    console.log(state)
    const navigate=useNavigate()
    return <div className={styles.filmInfor}>
        <p className={styles.label}>Nội dung phim</p>
        <div className={styles.filmDetails}>
            <img src={state.url.imgUrl} height="400px"/>
            <div className={styles.detailContent}>
                <p className={styles.filmName}>{state.name}</p>
                <p className={styles.propName}>Đạo diễn: <span>{state.director}</span></p>
                <p className={styles.propName}>Diễn viên: <span>{state.actor}</span></p>
                <p className={styles.propName}>Thể loại: <span>{state.type}</span></p>
                <p className={styles.propName}>Thời lượng: <span>{state.duration}</span></p>
                <p className={styles.propName}>Ngôn ngữ: <span>{state.lang}</span></p>
                <p className={styles.propName}>Nội dung: <span>{state.description}</span></p>
            </div>
        </div>
        <div className={styles.bookingArea}>
            <div className={styles.bookingButton} onClick={()=>{navigate(-1)}}>Trở về trang chủ</div>
            <div className={styles.bookingButton} onClick={()=>{navigate(`/${state.engName}/booking`,{state:state})}}>Đặt vé</div>
        </div>
    </div>
}