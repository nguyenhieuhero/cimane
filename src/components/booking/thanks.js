import styles from "./thanks.module.css"
import { useLocation, useNavigate } from "react-router-dom"
import logo from './thankslogo.png'
import sad from './sad.png'
export default function Thanks(){
    const {state} = useLocation();
    const userData=JSON.parse(localStorage.getItem("logincimane"))
    const navigate=useNavigate()
    console.log(userData)
    console.log(state)
    return <div className={styles.thanks}>
        <img src={state?logo:sad}/>
        {state?<h1>Thank you {userData.name} for purcharsing {state} VNĐ</h1>:
        <h1>Không mua thì bấm làm gì...</h1>}
        <div className={styles.confirmButton} onClick={()=>navigate("../")}>Xác nhận</div>
    </div>
}