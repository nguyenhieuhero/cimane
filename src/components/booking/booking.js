import styles from "./booking.module.css"
import Seat from "./seat"
import { useState,useEffect } from "react"
// import { fake } from "./fakeSeatData"
import { useLocation,useNavigate } from "react-router-dom"
import axios from "axios"
export default function Booking(){
    const {state} = useLocation();
    const navigate=useNavigate()
    const row=['A','B','C','D','E']
    const col=[1,2,3,4,5,6,7,8,9,10,11]
    const [numSeat,setNumSeat]=useState([])
    const [schedule,setSchedule]=useState(state.rooms&&state.rooms[0])
    const [notAvaiSeat,setNotAvaiSeat]=useState()
    const [data,setData]=useState()
    const getData = () => {
        axios.get(`http://localhost:8000/schedule/${Object.keys(schedule)[0]}`)
        .then(res => {
            setData(res.data)
            setNotAvaiSeat(res.data[0].seat[schedule[Object.keys(schedule)[0]]].seat)
        })
    }
    useEffect(()=>{
        getData()
        setNumSeat([])
    },[schedule])
    console.log(state)
    const handleSeat=(name)=>{
        if(notAvaiSeat?.includes(name)){
            alert("Ghế đã được đặt")
        }
        else{
            numSeat?.includes(name)?setNumSeat(numSeat.filter(item=>item!==name)):setNumSeat([...numSeat,name])
        }
    }
    console.log(notAvaiSeat)
    const handleChangeSchedule=(value)=>{
        // console.log(value)
        setSchedule(value)
    }
    const handleBooking=async schedule=>{
        if(!localStorage.getItem("logincimane")){
            alert("Bạn chưa đăng nhập!")
        }
        else{
            let request={
                room:Object.keys(schedule)[0],
                time:schedule[Object.keys(schedule)[0]],
                name:state.engName,
                seat:numSeat,
            }
            const res=await axios.post("http://localhost:8000/schedule/booking",request)
            if(res.data.success){
                navigate("/thanks",{state:numSeat?.length*65000})
            }
        }
    }
    console.log(state)
    // console.log(schedule)
    return <div className={styles.booking}>
        <div className={styles.wrapper}>
            <div className={styles.scheduleArea}>
                <h1 className={styles.scheduleText}>Lịch chiếu</h1>
                {state.rooms.map((value,index)=>
                <div className={value==schedule?styles.scheduleButtonChosen:styles.scheduleButton} key={index} onClick={()=>handleChangeSchedule(value)}>
                    <p>Phòng {Object.keys(value)[0]}</p>
                    <p>{value[Object.keys(value)[0]]}</p>
                </div>)}
            </div>
            <div className={styles.seatArea}>
                <img src='https://www.cgv.vn/skin/frontend/cgv/default/images/bg-cgv/bg-screen.png'/>
    
                {/* <button onClick={handleBooking}>Xác nhận</button> */}
                <div className={styles.seatRows}>
                    {row.map(row=><div key={row} className={styles.newRow}>
                        {col.map(name=><div key={row+name} onClick={()=>handleSeat(row+name)}>
                            <Seat key={name} name={row+name} notAvai={notAvaiSeat} isChosen={numSeat}/>
                            </div>)}
                        </div>)}
                </div>
            </div>
            <div className={styles.bookingArea}>
                <div className={styles.bookingInfor1}>
                <img src={state.url.imgUrl} height="300px"/>

                    <div className={styles.miniInfor1}>
                        <div className={styles.miniInfor2}>
                            <div className={styles.seatSample} style={{backgroundColor:"#1A2C50"}}/>
                            <p>Đang chọn</p>
                        </div>
                        <div className={styles.miniInfor2}>
                            <div className={styles.seatSample} style={{backgroundColor:"antiquewhite"}}/>
                            <p>Trống</p>
                        </div>
                        <div className={styles.miniInfor2}>
                            <div className={styles.seatSample} style={{backgroundColor:"gray"}}/>
                            <p>Đã chọn</p>
                        </div>
                    </div>
                </div>
                <h2>Bạn đã chọn {numSeat?.length} vé</h2>
                <h2>Thành tiền: {numSeat?.length*65000} VNĐ</h2>
                <div className={styles.confirmButton} onClick={()=>handleBooking(schedule)}>Xác nhận</div>      

            </div>
        </div>
    </div>
}