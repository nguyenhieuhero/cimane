import styles from './seat.module.css'
export default function Seat(prop) {
    // console.log(prop)
    return <div className={prop.notAvai?.includes(prop.name)?styles.seatActive:prop.isChosen?.includes(prop.name)?styles.seatPending:styles.seat}>
        <p>{prop.name}</p>
    </div>
}